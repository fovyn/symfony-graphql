<?php


namespace App\Resolver;


use App\Repository\AstronautRepository;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class AstronautByPseudoResolver implements ResolverInterface, AliasedInterface
{
    /** @var AstronautRepository $astronautRepository */
    private $astronautRepository;

    /**
     * AstronautByPseudoResolver constructor.
     * @param AstronautRepository $astronautRepository
     */
    public function __construct(AstronautRepository $astronautRepository)
    {
        $this->astronautRepository = $astronautRepository;
    }

    public function resolve(string $pseudo) {
        return $this->astronautRepository->findOneBy(["pseudo" => $pseudo]);
    }

    /**
     * @inheritDoc
     */
    public static function getAliases(): array
    {
        return [
            "resolve" => "AstronautByPseudo"
        ];
    }
}