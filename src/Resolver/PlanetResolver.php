<?php

namespace App\Resolver;

use App\Entity\Astronaut;
use App\Entity\Planet;
use App\Repository\PlanetRepository;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Psr\Log\LoggerInterface;


final class PlanetResolver implements ResolverInterface, AliasedInterface
{
    /**
     * @var PlanetRepository
     */
    private $planetRepository;
    /** @var LoggerInterface $logger */
    private $logger;

    /**
     *
     * @param PlanetRepository $planetRepository
     * @param LoggerInterface $logger
     */
    public function __construct(PlanetRepository $planetRepository, LoggerInterface $logger)
    {
        $this->planetRepository = $planetRepository;
        $this->logger = $logger;
    }

    /**
     * @param int $id
     * @return Planet
     */
    public function resolve(int $id)
    {
        return $this->planetRepository->find($id);
    }

    /**
     * @param Astronaut $astronaut
     * @param $args
     * @param $context
     * @param $info
     * @return Planet[]
     */
    public function resolveInAstronaut(Astronaut $astronaut, $args, $context, $info) {
        /** @var Planet[] $data */
        $data = $this->planetRepository->findByAstronaut($astronaut->getId());
        foreach ($data as $planet) {
            $this->logger->debug($planet->getName());
        }
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'Planet',
        ];
    }
}